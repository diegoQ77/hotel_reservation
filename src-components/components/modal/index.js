var backdrop = document.querySelector(".backdrop");
var modal = document.querySelector(".modal");
var modalNoButton = document.querySelector(".modal__action--negative");

backdrop.addEventListener("click", function() {
  closeModal();
});

if (modalNoButton) {
  modalNoButton.addEventListener("click", closeModal);
}

function closeModal() {
  if (modal) {
    modal.classList.remove("open");
  }
  backdrop.classList.remove("open");
}


let modalButton = document.querySelector("#modal-button")

modalButton.addEventListener("click", function(){
  console.log("clicl button modal")
  modal.classList.add("open");
  backdrop.classList.add("open");
})

