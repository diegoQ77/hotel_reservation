const main_nav = document.querySelector(".main-nav")
const mobile_nav = document.querySelector(".mobile-nav")

function load_nav(){
    let nav = `
        <ul class="main-nav__items">
            <li class="main-nav__item">
                <a href="../../../views/home/employee/employee.html">Home</a>
            </li>
            <li class="main-nav__item">
                <a href="../../../views_employee/customers/customer.html">Customers</a>
            </li>
            <li class="main-nav__item">
                <a href="../../../views_employee/reservation/reservations.html">Reservation</a>
            </li>

            <li class="main-nav__item">
                <a href="../../../views_employee/rooms/rooms.html">Rooms Status</a>
            </li>

            <li class="main-nav__item main-nav__item--cta">
                <a href="../../../views_employee/roomsAvailable/roomsAvailable.html">Available Rooms</a>
            </li>
        </ul>
        `;

    let mob_nav = `
        <ul class="mobile-nav__items">
            <li class="mobile-nav__item ">
                <a href="../../../views/home/employee/employee.html">Home</a>
            </li>

            <li class="mobile-nav__item">
                <a href="../../../views_employee/customers/customer.html">Customers</a>
            </li>

            <li class="main-nav__item">
                <a href="../../../views_employee/reservation/reservations.html">Reservation</a>
            </li>

            <li class="mobile-nav__item">
                <a href="../../../views_employee/rooms/rooms.html">Rooms Status</a>
            </li>

            <li class="mobile-nav__item mobile-nav__item--cta mobile-button">
                <a href="../../../views_employee/roomsAvailable/roomsAvailable.html">Available Rooms</a>
            </li>
        </ul>
        `;
    main_nav.innerHTML = nav;
    mobile_nav.innerHTML = mob_nav;
    
}
load_nav();