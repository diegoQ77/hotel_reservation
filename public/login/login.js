const form_login = document.querySelector("#form_login")
const login_msg = document.querySelector("#login-msg")
const login_url = "http://localhost:3000/users"


async function validate_login(username, password) {
    const db = await fetch(login_url);
    var response = await db.json();
    var user = false;
    var usuario = null
    
    for (let r of response){
        if (username == r.username && password == r.password){
            console.log("Validate user");
            user = true;
            usuario = r.username;
        };
    };
    console.log(usuario);
    if (user == false) {
        login_msg.innerHTML = "Invalid user";
    }else{
        console.log(usuario);
        window.location.href = "main/main.html?user="+usuario;
    }
}

function getDataForm(form) {
    const data = new FormData(form);
    return Object.fromEntries(data)
}

form_login.onsubmit = function (e) {
    e.preventDefault();
    const fdata = getDataForm(e.target);
    validate_login(fdata.username, fdata.password);
}