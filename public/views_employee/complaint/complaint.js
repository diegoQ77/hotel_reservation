
const data_url = "http://localhost:3000/rooms"

const form = document.querySelector('.form')
const button = document.querySelector('.save')

button.addEventListener('click', function(e){
  e.preventDefault()
  fomrData = getData() 
  saveData(fomrData)
})


async function saveData(data){
  const response = await fetch("http://localhost:3000/claims/", {
    method: "POST",
    headers: {"Content-type": "application/json"},
    body: JSON.stringify(data)
  })

  let data1 = await response.json()
  console.log(data1)

}


function getData(){
  const name = document.querySelector('#name')
  const id_reservation = document.querySelector('#id_reservation')
  const description = document.querySelector('#descriptionArea')

  return {
    'name' : name.value,
    'reservation' : id_reservation.value,
    'comment' : description.value,
    'status' : 'read'
  }
}

