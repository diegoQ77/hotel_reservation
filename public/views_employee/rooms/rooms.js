var backdrop = document.querySelector(".backdrop");
const id_table = document.querySelector("#data-table")
const data_url = "http://localhost:3000/rooms"
const table_filter = document.querySelector("#room-filter")
var filter = "all"

var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");


toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});

backdrop.addEventListener("click", function() {
  mobileNav.classList.remove("open");
  closeModal();
});


table_filter.onchange = function (e) {
  e.preventDefault();
  filter = e.target.value;
  loadDataTable(data_url);
}

async function loadDataTable(url) {
  const response = await fetch(url);
  var data = await response.json();
  show(data);
}

loadDataTable(data_url);


function getDataForm(form) {
  const data = new FormData(form);
  return Object.fromEntries(data)
}


function show(data) {
  let tab = 
      `
      <thead>
      <tr>
        <th>Id</th>
        <th>Type</th>
        <th>Pay per Day $us</th>
        <th>Status</th>
       </tr>
       </thead>

       <tbody>
       `;  
  let tfoot = 
      `</tbody>
       `;
  if (filter == "all"){
    for (let r of data) {
      tab += `<tr room-id='${r.id}'>
              <td>${r.id} </td>
              <td>${r.type}</td>
              <td>${r.price}</td> 
              <td>
                <button button-id='${r.id}' class='button-secondary action' id='modal-button'>
                ${r.status}
                </button>
              </td>          
          </tr>`;
    }
  }else{
    for (let r of data) {
      if(filter == r.status){
        tab += `<tr>
            <td>${r.id} </td>
            <td>${r.type}</td>
            <td>${r.price}</td> 
            <td>
            <button button-id='${r.id}' class='button-secondary action' id='modal-button'>
            ${r.status}
            </button>
          </td>          
          </tr>`; 
        }
      }
  }
  id_table.innerHTML = ""
  id_table.innerHTML = tab + tfoot;

  const button = document.querySelectorAll(".action");
  for (let i = 0; i < button.length; i++) {
    button[i].addEventListener("click", function() {
      openModal(i + 1)
    });
  }
}


// Update button

async function getRoom(room_id){
  const url = `http://localhost:3000/rooms/${room_id}`
  const response = await fetch(url);
  var data = await response.json();
  return data
}


const update = document.querySelector("#update");

function openModal(idRoom){
  modal.classList.add("open");
  // add idRoom
  update.dataset.room = idRoom
  backdrop.classList.add("open");
}

update.onclick = function(e) {
  updateState(e.target.dataset.room)
}

async function updateState(room_id){
  newState = document.querySelector('#select-option')
  newStatus = {'status' : newState.value}
  roomData = await getRoom(room_id).then(function(response){
    return response
  })

  newObj = {...roomData, ...newStatus}

  await fetch("http://localhost:3000/rooms/" + room_id, {
    method: "PUT",
    headers: {"Content-type": "application/json"},
    body: JSON.stringify(newObj)
  });

}


