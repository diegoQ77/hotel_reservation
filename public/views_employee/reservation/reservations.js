var backdrop = document.querySelector(".backdrop");
const id_table = document.querySelector("#data-table")
const data_url = "http://localhost:3000/reservations"

const table_filter = document.querySelector("#room-filter")
var filter = "all"

var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");


toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});

backdrop.addEventListener("click", function() {
  mobileNav.classList.remove("open");
  closeModal();
});


table_filter.onchange = function (e) {
  e.preventDefault();
  filter = e.target.value;
  console.log(filter);
  loadDataTable(data_url);
}

async function loadDataTable(url) {
  const response = await fetch(url);
  var data = await response.json();
  console.log(data);
  show(data);
}
loadDataTable(data_url);

function show(data) {
    let tab = 
        `
        <thead>
        <tr>
          <th>Id</th>
          <th>Customer</th>
          <th>Dni</th>
          <th>Phone</th>
          <th>Start date</th>
          <th>end date</th>
          <th>Total amount</th>
          <th>Check out</th>
         </tr>
         </thead>
  
         <tbody>
         `;  
    let endtab = 
        `</tbody>
         `;
    if (filter == "all"){
      for (let r of data) {
        tab += 
            `<tr room-id='${r.id}'>
                <td>${r.id} </td>
                <td>${r.customer}</td>
                <td>${r.dni}</td>
                <td>${r.phone}</td>
                <td>${r.start_date}</td>
                <td>${r.end_date}</td>
                <td>${r.total_amount}</td>
                <td>
                <a 
                  href=http://127.0.0.1:5501/public/views/check-out/check-out.html?id_reservation=${r.id}
                  button-id='${r.id}' 
                  class='a_link' 
                  id='moda'>
                  CHECK OUT
                </a>
              </td>      
            </tr>`;
      }
    }else{
      for (let r of data) {
        if(filter == r.status){
            tab += 
            `<tr room-id='${r.id}'>
              <td>${r.id} </td>
              <td>${r.customer}</td>
              <td>${r.dni}</td>
              <td>${r.phone}</td>
              <td>${r.start_date}</td>
              <td>${r.end_date}</td>
              <td>${r.total_amount}</td>
              <td>
              <a 
                href="http://127.0.0.1:5501/public/views/check-out/check-out.html" 
                button-id='${r.id}' 
                class='primary-button action' 
                id='moda'>
                CHECK OUT
              </a>
          </td>          
            </tr>`;    
        }
        }
    }
    id_table.innerHTML = ""
    id_table.innerHTML = tab + endtab;

  }