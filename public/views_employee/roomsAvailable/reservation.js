const values = window.location.search;
const urlParams = new URLSearchParams(values);

var  id = urlParams.get('id');
var  type = urlParams.get('type');

const package = document.querySelector("#idPackage")
package.value = `Id:${id} Type:${type}`


//CREATE:
const form_page = document.querySelector("#form_reservation")
var cbx = document.getElementsByName("services")
const payment_ini = document.querySelector("#payment_ini")
const total_amount = document.querySelector("#total_amount")

form_page.onsubmit = function (e) {
    e.preventDefault();
    const fdata = getDataForm(e.target);
    console.log(fdata)
    fetch("http://localhost:3000/reservations",{
        method: "POST",
        headers: {"Content-type": "application/json"},
        body: JSON.stringify(fdata)
    });
    window.location.href = "../reservation/reservations.html"
};
function getDataForm(form) {
    const data = new FormData(form);
    var services = []
    for (let i = 0; i < cbx.length; i++) {
        if(cbx[i].checked){
            services.push(cbx[i].value)
        }  
    }
    var pack = id
    var room = 0
    if(type == "room"){
        pack = 0
        room = id
    }
    var status = "pending"
    if (payment_ini == total_amount) {
        status = "completed"
    }
    data.append("services", services)
    data.append("room", room)
    data.append("package", pack)
    data.append("status", status)

    return Object.fromEntries(data)
}