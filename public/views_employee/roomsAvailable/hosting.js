const id_table = document.querySelector("#data_packages")
const data_url = "http://localhost:3000/packages"

async function loadDataOption(url) {
    const response = await fetch(url);
    var data = await response.json();
    console.log(data);
    show(data);
}
  
loadDataOption(data_url);

function show(data) {
    let tab = "";  
    for (let r of data) {
        var services = (r.services).split(',')
        var li = ""
        for (let s of services){
            li += `<li>${s}</li>`
        }

        tab += `
        <div class="card card__contianer-package">
            <div class="card__header-package">
                <h3 class="card__header-title">${r.name}</h3>
            </div>

            <div class="card__body">
                <h1>
                    $${r.price}/day
                </h1>

                <ul class="list-package">
                    ${li}
                    <li>Room ${r.rooms}</li>
                </ul>
                
                <p class="card__body-description">${r.description}</p>
                <p class="card__body-description">${r.creation_date} to ${r.expiration_date}</p>
            </div>

            <div class="card__button">
                <button button_id='${r.id}' button_type='package' class="button-primary reserve">
                    Reserve
                </button>
            </div>

            
        </div>    
        `;
                      
    }
    id_table.innerHTML = ""
    id_table.innerHTML = tab;

    const cbox = document.querySelectorAll(".reserve");

    for (let i = 0; i < cbox.length; i++) {
    cbox[i].addEventListener("click", function() {
      console.log("clicl button for", cbox[i].attributes.button_id.value)
      window.location.href = "reservation.html?id=" + cbox[i].attributes.button_id.value + "&type=" +  cbox[i].attributes.button_type.value
    //   tr_id = cbox[i].attributes.button_id.value
    //   modal.classList.add("open");
    //   backdrop.classList.add("open");
    });
  }
  
}
