let modalUpdate = document.querySelector(".updateModal");
var backdrop = document.querySelector(".backdrop");
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");
let modalCloseHeader = document.querySelector("#close-header-update")
let modalCloseButton = document.querySelector("#cancel-update")
const formPageModal = document.querySelector("#form-package-modal")


// HANDLE MODAL UPDATE
backdrop.addEventListener("click", function() {
  mobileNav.classList.remove("open");
  closeModal();
});

toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});

modalCloseHeader.addEventListener("click", function() {
  closeModal();
});

modalCloseButton.addEventListener("click", function() {
  closeModal();
});


function closeModal() {
  if (modalUpdate) {
    modalUpdate.classList.remove("open");
  }
  backdrop.classList.remove("open");
}


function getDataForm(form){
  const data = new FormData(form);
  var services = []
  for (let i = 0; i < cbx.length; i++) {
      if(cbx[i].checked){
          services.push(cbx[i].value)
      }  
  }
  const date = new Date();
  var fdate = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
  data.append("services", services)
  data.append("creation_date", fdate)

  return Object.fromEntries(data)
}


formPageModal.onsubmit = function (e) {
    e.preventDefault();
    fdata = getDataForm(e.target)
    fetch("http://localhost:3000/packages/" + currentModalUpdateId,{
        method: "PUT",
        headers: {"Content-type": "application/json"},
        body: JSON.stringify(fdata)
    });
    loadDataTable(data_url);
};