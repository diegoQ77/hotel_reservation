const form_page = document.querySelector("#form-package")
var cbx = document.getElementsByName("services")

form_page.onsubmit = function (e) {
    e.preventDefault();
    const fdata = getDataForm(e.target);
    console.log(fdata)
    fetch("http://localhost:3000/packages",{
        method: "POST",
        headers: {"Content-type": "application/json"},
        body: JSON.stringify(fdata)
    });
    loadDataTable(data_url);
};


function getDataForm(form) {
    const data = new FormData(form);
    var services = []
    for (let i = 0; i < cbx.length; i++) {
        if(cbx[i].checked){
            services.push(cbx[i].value)
        }  
    }
    const date = new Date();
    var fdate = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
    data.append("services", services)
    data.append("creation_date", fdate)
    return Object.fromEntries(data)
}

const id_table = document.querySelector("#data_table")
const data_url = "http://localhost:3000/packages"
const btn_delete = document.querySelector("#btn_delete")
var tr_id = 0 
var currentModalUpdateId = 0
let packageData = {}

async function loadDataTable(url) {
    const response = await fetch(url);
    var data = await response.json();
    show(data);
}
  
loadDataTable(data_url);


function show(data) {
    let tab = "";  
    for (let r of data) {
        var services = (r.services).split(',')
        var li = ""
        for (let s of services){
            li += `<li>${s}</li>`
        }

        tab += `
        <div class="card card__contianer-package">
            <div class="card__header-package">
                <h3 class="card__header-title">${r.name}</h3>
                <div class="card__header-actions">
                    <button button_id='${r.id}' class='primary-button delete'>
                        <span id="delete_package"></span>
                    </button>     
                </div>

                <div class="card__header-actions">
                    <button button_id='${r.id}' class='primary-button update'>
                        <span id="update_package"></span>
                    </button>     
                </div>
            </div>

            <div class="card__body">
                <h1>
                    $${r.price}/day
                </h1>

                <ul class="list-package">
                    ${li}
                    <li>Room ${r.rooms}</li>
                </ul>
                
                <p class="card__body-description">${r.description}</p>
                <p class="card__body-description">${r.creation_date} to ${r.expiration_date}</p>
            </div>


        </div>    
        `;
                      
    }
    id_table.innerHTML = ""
    id_table.innerHTML = tab;

    // ADD EVENT LISTNERS FOR DELETE
    const cbox = document.querySelectorAll(".delete");

    for (let i = 0; i < cbox.length; i++) {
        cbox[i].addEventListener("click", function() {
            console.log("clicl button modal for", cbox[i].attributes.button_id.value)
            tr_id = cbox[i].attributes.button_id.value
            modal.classList.add("open");
            backdrop.classList.add("open");
        });
    }


    // ADD EVENT LISTENERS FOR UPDATE
    const updateIcon = document.querySelectorAll(".update")
    for (let i = 0; i < updateIcon.length; i++) {
        updateIcon[i].addEventListener("click", function() {
            currentModalUpdateId = updateIcon[i].attributes.button_id.value
            modalUpdate.classList.add("open");
            backdrop.classList.add("open");
        });
    }
  
}



btn_delete.onclick = function(e) {
    console.log(tr_id)
    closeModal()
    fetch("http://localhost:3000/packages/" + tr_id, {
        method: "DELETE",
        headers: {"Content-type": "application/json"},
    });

    loadDataTable(data_url);
}


