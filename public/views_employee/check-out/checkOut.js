const queryString = window.location.search 
const urlParams = new URLSearchParams(queryString);
const id_reservation = urlParams.get('id_reservation')
const data_url = "http://localhost:3000/rooms"

// query selectors 
const nameSpan = document.querySelector('#name')
const reservationSpan = document.querySelector("#id_reservation")
const initalPaymentSpan = document.querySelector("#initial_payment")
// query selector total
const finalTotal = document.querySelector('#total-final')

// global variables
let totalExtras = 0
let finalPayment = 0
let reservationJson = {}


async function loadReservation(){
  const response = await fetch(`http://localhost:3000/reservations/${id_reservation}`)
  const reservation = await response.json()
  handleDataForm(reservation)
}

loadReservation()

function handleDataForm(data){
  nameSpan.innerHTML = data.customer
  reservationSpan.innerHTML = id_reservation
  initalPaymentSpan.innerHTML = data.payment_ini
  finalPayment = data.total_amount
  finalTotal.innerHTML = data.total_amount
  reservationJson = data

}

// Get query selector

const radioButtonCheckIn = document.querySelectorAll('input[name="check-in"]')
const radioBUttonCheckOut =  document.querySelectorAll('input[name="check-out"]')

// HANDLE CHECK IN CHANGE
for (let i = 0; i < radioButtonCheckIn.length; i++){
  radioButtonCheckIn[i].addEventListener('change', function(){
    if (radioButtonCheckIn[i].value == 1) {
      updateTotalNewAdd()
    }else{
      updateTotalNewSubtract()
    }
  })
}


function updateTotalNewAdd (){
  finalPayment = parseFloat(finalPayment) + 50
  finalTotal.innerHTML = finalPayment
}

function updateTotalNewSubtract (){
  finalPayment = parseFloat(finalPayment) - 50
  finalTotal.innerHTML = finalPayment
}

// HANDLE CHECK OUT CHANGE
for (let i = 0; i < radioBUttonCheckOut.length; i++){
  radioBUttonCheckOut[i].addEventListener('change', function(){
    if (radioBUttonCheckOut[i].value == 1) {
      updateTotalNewAdd()
    }else{
      updateTotalNewSubtract()
    }
    updateTotal(totalExtras)

  })
}

const dinnerExtra = document.querySelector("#dinnerExtra")
const poolExtra = document.querySelector("#poolExtra")
const wineExtra = document.querySelector("#wineExtra")
const carExtra = document.querySelector("#carExtra")

dinnerExtra.addEventListener('change', function(){
  if (dinnerExtra.checked){
    addExtra(25)

  }else{
    subTractExtra(25)
  }
})

poolExtra.addEventListener('change', function(){
  if (poolExtra.checked){
    addExtra(25)
  }else{
    subTractExtra(25)
  }

})

wineExtra.addEventListener('change', function(e){
  if (wineExtra.checked){
    addExtra(25)

  }else{
    subTractExtra(25)
  }

})

carExtra.addEventListener('change', function(e){
  if (carExtra.checked){
    addExtra(25)
  }else{
    subTractExtra(25)
  }

})


function updateTotal (totalExtras){
  finalPayment = parseFloat(totalExtras) + parseFloat(finalPayment)
  finalTotal.innerHTML = finalPayment
}

function addExtra(totalExtras){
  finalPayment = parseFloat(finalPayment) + parseFloat(totalExtras) 
  finalTotal.innerHTML = finalPayment
}

function subTractExtra(totalExtras){
  finalPayment = parseFloat(finalPayment) - parseFloat(totalExtras) 
  finalTotal.innerHTML = finalPayment
}


const form = document.querySelector(".form")


form.onsubmit = function (e) {
  e.preventDefault();
  updateReservation()
  window.location.href = "http://127.0.0.1:5501/public/views/reservation/reservations.html"
}

form.addEventListener('submit', updateReservation)


async function updateReservation(){
  newData = {...reservationJson, ...{"status":"Payment", "total_amount": finalPayment}}
  await fetch("http://localhost:3000/reservations/" + id_reservation, {
    method: "PUT",
    headers: {"Content-type": "application/json"},
    body: JSON.stringify(newData)
  });
}




