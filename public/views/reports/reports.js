// for nav menu mobile
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");
var backdrop = document.querySelector(".backdrop");

backdrop.addEventListener("click", function() {
  mobileNav.classList.remove("open");
  closeModal();
});

toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});

const id_table = document.querySelector("#data_general")
const data_url = "http://localhost:3000/reservations"

const general_filter = document.querySelector("#general_filter")
var gnrl_filter = "annual"

general_filter.onchange = function (e) {
  e.preventDefault();
  gnrl_filter = e.target.value;
  loadDataTable(data_url);
}


async function loadDataTable(url) {
  const response = await fetch(url);
  var data = await response.json();
  //console.log(data);
  show(data);
}
loadDataTable(data_url);



function show(data) {
  let tab = 
      `
      <thead>
        <tr>
          <th>NR</th>
          <th>Date filter</th>
          <th>Packages</th>
          <th>Rooms</th>
          <th>Amount</th>
        </tr>
       </thead>
       <tbody>
       `;  
  let tfoot = ''
  var total = 0

    years = []
    for (let r of data) {
      total += parseInt(r.total_amount, 10)
      let end_date = new Date(r.end_date)

      if (gnrl_filter == "annual"){
        years.push(end_date.getFullYear())
      }
      if (gnrl_filter == "monthly"){
        years.push(end_date.getFullYear()+"-"+(end_date.getMonth() +1))
      }
      if (gnrl_filter == "weekly"){
        //years.push(end_date.getFullYear()+"-"+(end_date.getMonth() +1))
        console.log(end_date.getDate())
      }
      

    }
    
    const dataArr = new Set(years);
    var result = [...dataArr];
    console.log("Date filter: "+result)

    for (let i = 0; i < result.length; i++) {
      let rooms = 0
      let packages = 0
      var subtotal = 0
      for(let r of data){
        let date_filter = ""
        if (gnrl_filter == "annual"){
          let end_date = new Date(r.end_date)
          date_filter = end_date.getFullYear()
        }
        if (gnrl_filter == "monthly"){
          let end_date = new Date(r.end_date)
          date_filter = end_date.getFullYear()+"-"+(end_date.getMonth() + 1)  
        }

        //let end_date = new Date(r.end_date)


        //if(result[i] == end_date.getFullYear()){
        if(result[i] == date_filter){
          subtotal += parseInt(r.total_amount, 10)
          if(r.package > 0){
            packages ++
          }
          if(r.room > 0){
            rooms ++
          } 
        }
      }
      tab += `
        <tr>
          <td>${i + 1}</td>
          <td>${result[i]}</td>
          <td>${packages}</td>
          <td>${rooms}</td>
          <td>${subtotal}</td>           
        </tr>`;
    }

  tfoot = 
      `
      </tbody>
      <tfoot>
        <tr>
          <td id="footer" colspan="4">Total amount</td>
          <td id="total">${total}</td>
        </tr>
      </tfoot>
       `;
  id_table.innerHTML = tab + tfoot;
}





// function show(data) {
//   let tab = 
//       `
//       <thead>
//         <tr>
//           <th>Id</th>
//           <th>Customer</th>
//           <th>DNI - Phone</th>
//           <th>Reservation Type</th>
//           <th>Room type</th>
//           <th>Amount</th>
//         </tr>
//        </thead>
//        <tbody>
//        `;  
//   let tfoot = ''
//   var total = 0
//   if (gnrl_filter == "all" || gnrl_filter == "weekly"){   
//     for (let r of data) {
      
//       total += parseInt(r.total_amount, 10)
//       let type = 'Package'
//       if(r.room == 1){
//         type = 'Room'
//       }
//       tab += `
//         <tr>
//           <td>${r.id} </td>
//           <td>${r.customer}</td>
//           <td>${r.dni} - ${r.phone}</td>
//           <td>${type}</td> 
//           <td>${r.rooms}</td>
//           <td>${r.total_amount}</td>          
//         </tr>`;
//     }
//   }
//   if(gnrl_filter == "annual"){
//     tab = 
//       `
//       <thead>
//         <tr>
//           <th>Nr</th>
//           <th>Year</th>
//           <th>Rooms</th>
//           <th>Packages</th>
//           <th></th>
//           <th>Amount</th>
//         </tr>
//        </thead>
//        <tbody>
//        `;
//     let packages = 0
//     let rooms = 0

//     let partial_packages = 0
//     let partial_rooms = 0
//     var years = []
//     for (let r of data) {
//       var end_date = new Date(r.end_date)
//       years.push(end_date.getFullYear())

//       total += parseInt(r.total_amount, 10)
//       if(r.room == 1){
//         rooms += 1;
//         partial_rooms += parseInt(r.total_amount, 10)
//       }
//       if(r.package == 1){
//         packages += 1;
//         partial_packages += parseInt(r.total_amount, 10)
//       }
//     }
//     const dataArr = new Set(years);
//     var result = [...dataArr];

//     for (let i = 0; i < result.length; i++) {
//       tab += `
//         <tr>
//           <td>${i+1} </td>
//           <td>${result[i]}</td>
//           <td></td>
//           <td></td> 
//           <td></td>
//           <td></td>           
//         </tr>`;
//     }

//   }
//   tfoot = 
//       `
//       </tbody>
//       <tfoot>
//         <tr>
//           <td id="footer" colspan="5">Total amount</td>
//           <td id="total">${total}</td>
//         </tr>
//       </tfoot>
//        `;
//   id_table.innerHTML = tab + tfoot;
// }
