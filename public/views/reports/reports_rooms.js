const id_table_rooms = document.querySelector("#data_rooms")
const data_url_rooms = "http://localhost:3000/reservations"

const rooms_filter = document.querySelector("#rooms_filter")
var rm_filter = "packages"

const button_search_room = document.querySelector('#search_room')
const search_input = document.querySelector('#search_input')

var id_room = 0

button_search_room.onclick = function(e) {
  e.preventDefault();
  id_room = search_input.value
  loadDataRooms(data_url_rooms);
}

rooms_filter.onchange = function (e) {
  e.preventDefault();
  rm_filter = e.target.value;
  loadDataRooms(data_url_rooms);
}


async function loadDataRooms(url) {
  const response = await fetch(url);
  var data = await response.json();
  console.log(data);
  show_rooms(data);
}
loadDataRooms(data_url_rooms);

function show_rooms(data) {
  let tab = 
      `
      <thead>
        <tr>
          <th>ID</th>
          <th>Customer</th>
          <th>DNI - Phone</th>
          <th>Reservation Type</th>
          <th>Room type</th>
          <th>Amount</th>
        </tr>
       </thead>
       <tbody>
       `;  
  var total = 0

  if (rm_filter == "packages"){
    total = 0   
    for (let r of data) {

      if(parseInt(r.package, 10) > 0){
        total += parseInt(r.total_amount, 10)
        let type = 'Package'

        if(id_room == 0 || id_room == r.id){
        tab += `
          <tr>
            <td>${r.id} </td>
            <td>${r.customer}</td>
            <td>${r.dni} - ${r.phone}</td>
            <td>${type}</td> 
            <td>${r.rooms}</td>
            <td>${r.total_amount}</td>          
          </tr>`;
        }
      }
    }
  }

  if (rm_filter == "rooms"){ 
    total = 0   
    for (let r of data) {

      if(parseInt(r.room, 10) > 0){
        total += parseInt(r.total_amount, 10)
        let type = 'Room'

        if(id_room == 0 || id_room == r.id){
        tab += `
          <tr>
            <td>${r.id} </td>
            <td>${r.customer}</td>
            <td>${r.dni} - ${r.phone}</td>
            <td>${type}</td> 
            <td>${r.rooms}</td>
            <td>${r.total_amount}</td>          
          </tr>`;
        }
      }
    }
  }

  let tfoot = 
      `
      </tbody>
      <tfoot>
        <tr>
          <td id="footer" colspan="5">Total amount</td>
          <td id="total">${total}</td>
        </tr>
      </tfoot>
      `;
  id_table_rooms.innerHTML = ""
  id_table_rooms.innerHTML = tab + tfoot;
}






