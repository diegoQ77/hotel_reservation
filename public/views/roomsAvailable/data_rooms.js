const id_table_rooms = document.querySelector("#data_rooms")
const data_url_rooms = "http://localhost:3000/rooms"
var backdrop = document.querySelector(".backdrop");
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");


toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});

backdrop.addEventListener("click", function() {
    console.log("click fuera");
  mobileNav.classList.remove("open");
  closeModal();
});

const available_filter = document.querySelector("#available_filter");
var av_filter = "all"

available_filter.onchange = function(e) {
    av_filter = e.target.value
    loadDataRooms(data_url_rooms);
}


async function loadDataRooms(url) {
    const response = await fetch(url);
    var data = await response.json();
    console.log(data);
    show_rooms(data);
}
  
loadDataRooms(data_url_rooms);

function show_rooms(data) {
    let tab = "";
    if(av_filter == "all") {
        for (let r of data) {
            if(r.status == 'vacancy'){
            tab += `
            <div class="card card__reserve">
                <div class="card__image">
                    <img src="../../img/suit.jpg" alt="">
                </div>
            
                <div class="card__body-reserve">
                    <div class="card__body-head">
                        <div class="card__head-title">
                            <h2 id="type-room">${r.type}</h2>
                            <span>2 Bed / 2 People / Pool</span>
                        </div>
                        <div class="card__head-price">
                            <h3 id="price_room">$${r.price}/day</h3>
                            <h3>Free</h3>
                        </div>
                    </div>
                    <hr><br>
                    <div class="card__body-information">
                        <p>${r.detail}</p>
                        <div class="card__body checkbox">
                            <ul class="list-package">
                                <li>Cena</li>
                                <li>Desayuno</li>
                                <li>ALmuerzo</li>
                                <li>Habitacion Suit</li>
                            </ul>
                        </div>
                    </div>

                    <div class="card__button">
                    <button button_id='${r.id}' button_type='room' class="button-primary reserve">Reserve</button>
                    </div>
                </div>
            </div>
            `;
            }                 
        }
    }else{
        for (let r of data) {
            if(av_filter == r.type){
                if(r.status == 'vacancy'){
                tab += `
                    <div class="card card__reserve">
                        <div class="card__image">
                            <img src="../../img/suit.jpg" alt="">
                        </div>
                    
                        <div class="card__body-reserve">
                            <div class="card__body-head">
                                <div class="card__head-title">
                                    <h2 id="type-room">${r.type}</h2>
                                    <span>2 Bed / 2 People / Pool</span>
                                </div>
                                <div class="card__head-price">
                                    <h3 id="price_room">$${r.price}/day</h3>
                                    <h3>Free</h3>
                                </div>
                            </div>
                            <hr><br>
                            <div class="card__body-information">
                                <p>${r.detail}</p>
                                <div class="card__body checkbox">
                                    <ul class="list-package">
                                        <li>Cena</li>
                                        <li>Desayuno</li>
                                        <li>ALmuerzo</li>
                                        <li>Habitacion Suit</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="card__button">
                            <button button_id='${r.id}' button_type='room' class="button-primary reserve">Reserve</button>
                            </div>
                        </div>
                    </div>
                    `;
                }  
            }               
        }
    }
    id_table_rooms.innerHTML = ""
    id_table_rooms.innerHTML = tab;

    if(id_table_rooms.innerHTML == ""){
        id_table_rooms.innerHTML = "No available rooms"
    }

    const cbox = document.querySelectorAll(".reserve");

    for (let i = 0; i < cbox.length; i++) {
    cbox[i].addEventListener("click", function() {
      console.log("clicl button for", cbox[i].attributes.button_id.value)
      window.location.href = "reservation.html?id=" + cbox[i].attributes.button_id.value + "&type=" +  cbox[i].attributes.button_type.value
    });
  }
  
}
