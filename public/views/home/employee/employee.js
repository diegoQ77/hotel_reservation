var backdrop = document.querySelector(".backdrop");
var modal = document.querySelector(".modal");
var modalNoButton = document.querySelector(".modal__action--negative");
var selectPlanButtons = document.querySelectorAll(".plan button");
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");
const data_url = "http://localhost:3000/reservations"
const totalEarning = document.querySelector("#total_amount")
const totalCustomers = document.querySelector("#total_customer")



for (var i = 0; i < selectPlanButtons.length; i++) {
  selectPlanButtons[i].addEventListener("click", function() {
    modal.classList.add("open");
    backdrop.classList.add("open");
  });
}

backdrop.addEventListener("click", function() {
  mobileNav.classList.remove("open");
  closeModal();
});

if (modalNoButton) {
  modalNoButton.addEventListener("click", closeModal);
}

function closeModal() {
  if (modal) {
    modal.classList.remove("open");
  }
  backdrop.classList.remove("open");
}

toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});



async function loadData(url) {
  const response = await fetch(url);
  var data = await response.json();
  const package = await fetch('http://localhost:3000/packages');
  var dataPackage = await package.json();
  total_amount = get_total(data)
  totalEarning.innerHTML = total_amount
  totalCustomers.innerHTML = data.length
}
loadData(data_url);

function get_total(data){
  let sum = 0
  data.forEach(element => {
    sum += parseFloat(element.total_amount)
  });
  return sum
}