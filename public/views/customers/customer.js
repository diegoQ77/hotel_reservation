var backdrop = document.querySelector(".backdrop");
var modal = document.querySelector(".modal");
var modalNoButton = document.querySelector(".modal__action--negative");
var selectPlanButtons = document.querySelectorAll(".plan button");
var toggleButton = document.querySelector(".toggle-button");
var mobileNav = document.querySelector(".mobile-nav");

for (var i = 0; i < selectPlanButtons.length; i++) {
  selectPlanButtons[i].addEventListener("click", function() {
    modal.classList.add("open");
    backdrop.classList.add("open");
  });
}

backdrop.addEventListener("click", function() {
  mobileNav.classList.remove("open");
  closeModal();
});

if (modalNoButton) {
  modalNoButton.addEventListener("click", closeModal);
}

function closeModal() {
  if (modal) {
    modal.classList.remove("open");
  }
  backdrop.classList.remove("open");
}

toggleButton.addEventListener("click", function() {
  mobileNav.classList.add("open");
  backdrop.classList.add("open");
});

const id_table = document.querySelector("#data-table")
const data_url = "http://localhost:3000/reservations"

async function loadDataTable(url) {
  const response = await fetch(url);
  var data = await response.json();
  console.log(data);
  show(data);
}

loadDataTable(data_url);

function show(data) {
  let tab = 
      `
      <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Dni</th>
        <th>Phone</th>
       </tr>
       </thead>

       <tbody>
       `;  
  let tfoot = 
      `
      </tbody>
      <tfoot>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Dni</th>
        <th>Phone</th>
      </tr>
      </tfoot>
       `;

  for (let r of data) {
      tab += `<tr>
              <td>${r.id} </td>
              <td>${r.customer}</td>
              <td>${r.dni}</td> 
              <td>${r.phone}</td>          
          </tr>`;
  }
  id_table.innerHTML = tab;
}

const id_table_claim = document.querySelector("#data-table-claims")
const data_url_claim = "http://localhost:3000/claims"

async function loadDataTable_two(url) {
  const response = await fetch(url);
  var data = await response.json();
  console.log(data);
  show_claim(data);
}

loadDataTable_two(data_url_claim);

function show_claim(data) {
  let tab = 
      `
      <thead>
      <tr>
        <th>Id</th>
        <th>Reservation</th>
        <th>Comment</th>
        <th>Status</th>
       </tr>
       </thead>

       <tbody>
       `;  
  let tfoot = 
      `
      </tbody>
      <tfoot>
      <tr>
        <th>Id</th>
        <th>Reservation</th>
        <th>Comment</th>
        <th>Status</th>
      </tr>
      </tfoot>
       `;

  for (let r of data) {
      tab += `<tr>
              <td>${r.id} </td>
              <td>${r.reservation}</td>
              <td>${r.comment}</td> 
              <td>${r.status}</td>          
          </tr>`;
  }
  id_table_claim.innerHTML = tab;
}