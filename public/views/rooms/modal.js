var backdrop = document.querySelector(".backdrop");
var modal = document.querySelector(".modal");
var modalNoButton = document.querySelector(".modal__action--negative");
let modalCloseHeader = document.querySelector("#close-header")
let cancel = document.querySelector("#cancel")

// Event listeners
backdrop.addEventListener("click", function() {
  closeModal();
});

modalCloseHeader.addEventListener("click", function() {
  closeModal();
});

cancel.addEventListener("click", function() {
  closeModal();
});


if (modalNoButton) {
  modalNoButton.addEventListener("click", closeModal);
}

function closeModal() {
  if (modal) {
    modal.classList.remove("open");
  }
  backdrop.classList.remove("open");
}


