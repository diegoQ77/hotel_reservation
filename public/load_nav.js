const main_nav = document.querySelector(".main-nav")
const mobile_nav = document.querySelector(".mobile-nav")

function load_nav(){
    let nav = `
        <ul class="main-nav__items">
            <li class="main-nav__item">
            <a href="../customers/customer.html">Customers</a>
            </li>

            <li class="main-nav__item">
            <a href="../reservations/reservations.html">Reservations</a>
            </li>

            <li class="main-nav__item">
            <a href="../packages/packages.html">Packages</a>
            </li>

            <li class="main-nav__item main-nav__item--cta">
            <a href="start-hosting/index.html">Create package</a>
            </li>
        </ul>
        `;

    let mob_nav = `
        <ul class="mobile-nav__items">
            <li class="mobile-nav__item">
            <a href="../customers/customer.html">Customers</a>
            </li>

            <li class="mobile-nav__item">
            <a href="../reservation/reservation.html">Reservations</a>
            </li>

            <li class="mobile-nav__item">
            <a href="../package/package.html">Packages</a>
            </li>

            <li class="mobile-nav__item mobile-nav__item--cta mobile-button">
            <a href="start-hosting/index.html">Create package</a>
            </li>
        </ul>
        `;
    main_nav.innerHTML = nav;
    mobile_nav.innerHTML = mob_nav;
    
}
load_nav();