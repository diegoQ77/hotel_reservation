
# Definition of Done

* All user stories are described and have acceptance criteria.
* The acceptance criteria are described in the User Stories and completed.
* The user stories completed the development phases.
* The code review of each functionality or module of the project was carried out.
* Data for reservations, packages, and other information have to be recorded in a json file.
* The project is in a repository on gitlab.
* Each webpage must be responsive for mobile and desktop sizes.
* The inputs of the forms must be validated to enter information in the mandatory fields and consistent information.

# Definition of ready

* User stories are understandable and valued with story points
* User stories are described and have acceptance criteria.
* Each user story has specific tasks
* Task must not have any block for its execution
* The user stories were reviewed by the development team

